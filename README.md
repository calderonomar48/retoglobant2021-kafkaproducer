# Spring Boot with Kafka Producer Example

This Project covers how to use Spring Boot with Spring Kafka to Publish JSON/String message to a Kafka topic
## Start Zookeeper
- `bin/zookeeper-server-start.sh config/zookeeper.properties`

## Start Kafka Server
- `bin/kafka-server-start.sh config/server.properties`

## Create Kafka Topic
- `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Kafka_Example`

## Consume from the Kafka Topic via Console
- `bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic Kafka_Example --from-beginning`

## Publish message via WebService
- `http://localhost:8081/kafka/publish/Sam`
- `http://localhost:8081/kafka/publish/Peter`


# Comandos Kafka

- iniciar el server del zookeeper<br />
kafka_2.13-2.8.0/bin/zookeeper-server-start.sh kafka_2.13-2.8.0/config/zookeeper.properties
<br />
- iniciar el server de kafka<br />
kafka_2.13-2.8.0/bin/kafka-server-start.sh kafka_2.13-2.8.0/config/server.properties
<br />
- crear un topico<br />
sh kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic-demo
<br />
- listar los topicos creados<br />
sh kafka-topics.sh --list --zookeeper localhost:2181
<br />
- delete topics<br />
sh kafka-topics.sh --delete --zookeeper localhost:2181 --topic demo
<br />
- ver detalle de un topic<br />
sh kafka-topics.sh --describe --zookeeper localhost:2181 --topic demo
<br />


# Ejercicio kafka
-- levantar un consumer kafka <br />
sh kafka-console-consumer.sh --bootstrap-server localhost:9092 --from-beginning --topic topic-demo
sh kafka-console-consumer.sh --bootstrap-server localhost:9092 --from-beginning --topic kafka_globant_json

<br />
-- Arrancar un productor por consola<br />
sh kafka-console-producer.sh --broker-list localhost:9092 --topic topic-demo
<br />

sh kafka-console-consumer.sh --bootstrap-server localhost:9092 --from-beginning --topic Kafka_Example --group group_id
sh kafka-console-producer.sh --broker-list localhost:9092 --topic Kafka_Example

sh kafka-console-producer.sh --broker-list localhost:9092 --topic Kafka_Example_json
sh kafka-topics.sh --delete --zookeeper localhost:2181 --topic Kafka_Example_json