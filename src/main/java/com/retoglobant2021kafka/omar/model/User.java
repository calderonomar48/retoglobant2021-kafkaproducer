package com.retoglobant2021kafka.omar.model;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 30/09/2021
 * Time: 12:12 p. m.
 * Change Text
 */
@Data
@Builder
public class User {

  private String name;

  private String dept;

  private Long salary;

  public User() {
  }

  public User(String name, String dept, Long salary) {
    this.name = name;
    this.dept = dept;
    this.salary = salary;
  }
}
