package com.retoglobant2021kafka.omar.userKafka;

import com.retoglobant2021kafka.omar.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 30/09/2021
 * Time: 11:31 a. m.
 * Change Text
 */
@RestController
@RequestMapping("kafka")
public class ProducerUser {

  @Autowired
  private KafkaTemplate<String, User> kafkaTemplate;

  private static final String TOPIC_NAME = "kafka_globant_json";

  @GetMapping("/publish/{text}")
  public String sendMessage(@PathVariable("text") final String text){
    kafkaTemplate.send(TOPIC_NAME,
            User.builder()
              .name(text)
              .dept("Department store")
              .salary(2000l).build());
    return "publish ok";
  }
}
